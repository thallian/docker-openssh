[OpenSSH](https://www.openssh.com/) server.

# Volumes
- `/etc/ssh/keys`

# Ports
- 22
