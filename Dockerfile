FROM registry.gitlab.com/thallian/docker-alpine-s6:master

RUN apk add --no-cache openssh

ADD /rootfs /

EXPOSE 22

VOLUME /etc/ssh/keys/
